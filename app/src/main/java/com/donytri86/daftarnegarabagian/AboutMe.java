package com.donytri86.daftarnegarabagian;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class AboutMe extends AppCompatActivity {

    private static String aboutName = "Dony Tri Prasetya";
    private static String aboutTitle = "donytri86@gmail.com";
    private static String aboutDetail = "A highly-skilled Software Engineer with a demonstrated history of working in the information technology and services industry. A strong believer in the ability of PHP (Native, Laravel, Codeigniter), E-commerce, and Software System Analysis.";
    private static int aboutPhoto = R.drawable.about_me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);

        ImageView imgPhoto;
        TextView tvName, tvTitle, tvDetail;

        imgPhoto = findViewById(R.id.img_item_photo);
        tvName = findViewById(R.id.tv_item_name);
        tvTitle = findViewById(R.id.tv_item_title);
        tvDetail = findViewById(R.id.tv_item_detail);

        tvName.setText(aboutName);
        tvTitle.setText(aboutTitle);
        tvDetail.setText(aboutDetail);
        Glide.with(this)
                .load(aboutPhoto)
                .apply(new RequestOptions().override(300, 300))
                .into(imgPhoto);
    }
}