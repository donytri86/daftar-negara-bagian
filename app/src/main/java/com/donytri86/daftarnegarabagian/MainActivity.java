package com.donytri86.daftarnegarabagian;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity<onCreateOptionMenu> extends AppCompatActivity {

    private RecyclerView rvCasts;
    private ArrayList<State> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvCasts = findViewById(R.id.rv_cast);
        rvCasts.setHasFixedSize(true);

        list.addAll(StateData.getListData());
        showRecyclerList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_about, menu);

        return true;
    }

    private void showRecyclerList(){
        rvCasts.setLayoutManager(new LinearLayoutManager(this));
        ListStateAdapter listStateAdapter = new ListStateAdapter(list);
        rvCasts.setAdapter(listStateAdapter);

        listStateAdapter.setOnItemClickCallback(new ListStateAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(State state) {
                showSelectedHero(state);
            }
        });
    }

    private void showSelectedHero(State state) {
        Intent intent = new Intent(MainActivity.this, StateDetail.class);
        intent.putExtra("name", state.getName());
        intent.putExtra("capital", state.getCapital());
        intent.putExtra("detail", state.getDetail());
        intent.putExtra("photo", state.getPhoto());
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_about) {
            Intent intent = new Intent(MainActivity.this, AboutMe.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}