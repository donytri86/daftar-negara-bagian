package com.donytri86.daftarnegarabagian;

public class State {
    private String name;
    private String capital;
    private String detail;
    private int photo;

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name; }

    public String getCapital() { return capital; }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getDetail() {
        return detail;
    }

    public String getDetailTiny() {
        return detail.substring(0, 100);
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}
