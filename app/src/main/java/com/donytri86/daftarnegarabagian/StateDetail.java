package com.donytri86.daftarnegarabagian;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class StateDetail extends AppCompatActivity {
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_detail);

        ImageView imgPhoto;
        TextView tvName, tvDetail, tvCapital;

        imgPhoto = findViewById(R.id.img_item_photo);
        tvName = findViewById(R.id.tv_item_name);
        tvCapital = findViewById(R.id.tv_item_capital);
        tvDetail = findViewById(R.id.tv_item_detail);

        String name = getIntent().getStringExtra("name");
        String capital = getIntent().getStringExtra("capital");
        String detail = getIntent().getStringExtra("detail");
        int photo = getIntent().getIntExtra("photo", 0);

        tvName.setText(name);
        tvCapital.setText(capital);
        tvDetail.setText(detail);
        tvDetail.setMovementMethod(new ScrollingMovementMethod());

        Glide.with(this)
                .load(photo)
                .apply(new RequestOptions().override(300, 300))
                .into(imgPhoto);
    }
}